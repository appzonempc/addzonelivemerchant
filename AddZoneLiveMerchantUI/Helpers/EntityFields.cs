﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddZoneLiveMerchant
{
    #region Service Entities
    public partial class Entities
    {
        public const string ZONE_MERCHANT_SERVICE = "Zone Merchant Service";
        public const string ZONE_MERCHANT_LIVE = "Zone Merchant Live";
        public const string ZONE_TERMINAL = "Zone Terminal";
        public const string MERCHANT_TERMINAL = "Merchant Terminal";


    }


    #endregion

    #region Entity Fields for Service Entities
    public class EntityFieldsForZoneMerchantService
    {
        ///<summary>
        /// PARAMETER FORMAT: "Integer"
        ///</summary>
        public const string ID = "ID";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string QUICKTELLER_BILLER_ID = "Quickteller Biller ID";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string CURRENCY_CODE = "Currency Code";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string CURRENCY_SYMBOL = "Currency Symbol";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string CUSTOMER_FIELD = "Customer Field";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string NAME = "Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string NARRATION = "Narration";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string SHORT_NAME = "Short Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string BILLER_CATEGORY_ID = "Biller Category ID";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string SERVICE_FUNCTION = "Service Function";

        ///<summary>
        /// PARAMETER FORMAT: "Boolean"
        ///</summary>
        public const string IS_ZONE_SERVICE = "Is Zone Service";

    }

    public class EntityFieldsForZoneMerchantLive
    {
        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string UPDATEMESSAGE = "UpdateMessage";


        ///<summary>
        /// PARAMETER FORMAT: "Integer"
        ///</summary>
        public const string MERCHANT_ID = "Merchant Id";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string MERCHANT_NAME = "Merchant Name";

        ///<summary>
        /// PARAMETER FORMAT: "Integer"
        ///</summary>
        public const string MERCHANT_CODE = "Merchant Code";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string MERCHANT_DETAILS = "Merchant Details";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string LOGO = "Logo";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string BUSINESS_ADDRESS = "Business Address";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string SUPPORT_PHONE_NUMBER = "Support Phone Number";

        ///<summary>
        /// PARAMETER FORMAT: "Email"
        ///</summary>
        public const string SUPPORT_EMAIL_ADDRESS = "Support Email Address";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string CONTACT_FIRST_NAME = "Contact First Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string CONTACT_LAST_NAME = "Contact Last Name";

        ///<summary>
        /// PARAMETER FORMAT: "Email"
        ///</summary>
        public const string CONTACT_EMAIL_ADDRESS = "Contact Email Address";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string CONTACT_PHONE_NUMBER = "Contact Phone Number";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string WEBSITE_URL = "Website URL";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string SUCCESS_RETURN_URL = "Success Return URL";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string FAILURE_RETURN_URL = "Failure Return URL";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string PENDING_RETURN_URL = "Pending Return URL";

        ///<summary>
        /// PARAMETER FORMAT: "Json Override"
        ///</summary>
        public const string LIST_OF_TERMINALS = "List of Terminals";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string FEE = "Fee";

        ///<summary>
        /// PARAMETER FORMAT: "Json Override"
        ///</summary>
        public const string SERVICES = "Services";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string SETTLEMENT_ACCOUNT_NAME = "Settlement Account Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string SETTLEMENT_ACCOUNT_NUMBER = "Settlement Account Number";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string SETTLEMENT_ACCOUNT_BANK = "Settlement Account Bank";

        ///<summary>
        /// PARAMETER FORMAT: "Boolean"
        ///</summary>
        public const string IS_DOWNLOADED_AT_SETUP = "Is Downloaded at Setup";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string FRIENDLY_NAME = "FriendlyName";
        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string WELCOMEMESSAGE = "WelcomeMessage";


    }

    #endregion

    #region For Entities that don't belong to the Service but needed by the Command

    #endregion

    #region The Rest
    public partial class Entities
    {
        public const string CLIENT_FUNCTION = "Client Function";
    }

    public class EntityFieldsForZoneTerminal
    {
        public const string TERMINAL_NAME = "Terminal Name";
        public const string TERMINAL_ID = "Terminal Id";
        public const string IS_ASSIGNED_TO_MERCHANT = "IsAssignedToMerchant";
    }

    public class EntityFieldsForClientFunction
    {
        ///<summary>
        /// PARAMETER FORMAT: "AlphaNumeric"
        ///</summary>
        public const string NAME = "Name";

        ///<summary>
        /// PARAMETER FORMAT: "AlphaNumeric"
        ///</summary>
        public const string DESCRIPTION = "Description";

        ///<summary>
        /// PARAMETER FORMAT: "AlphaNumeric"
        ///</summary>
        public const string INITIATION_MODE = "Initiation Mode";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string FLOW_GUID = "Flow Guid";

        ///<summary>
        /// PARAMETER FORMAT: "AlphaNumeric"
        ///</summary>
        public const string SMALL_ICON_URL = "Small Icon Url";

        ///<summary>
        /// PARAMETER FORMAT: "AlphaNumeric"
        ///</summary>
        public const string BACK_HOME_SCREEN_IMAGE_URL = "Back Home Screen Image Url";

        ///<summary>
        /// PARAMETER FORMAT: "AlphaNumeric"
        ///</summary>
        public const string FRONT_HOME_SCREEN_IMAGE_URL = "Front Home Screen Image Url";

        ///<summary>
        /// PARAMETER FORMAT: "Integer"
        ///</summary>
        public const string MAIN_SERVICE_ID = "Main Service ID";

        ///<summary>
        /// PARAMETER FORMAT: "Boolean"
        ///</summary>
        public const string IS_ZONE_FUNCTION = "Is Zone Function";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string CLIENT_FUNCTION_CATEGORY = "Client Function Category";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string HISTORY_DESCRIPTION = "History Description";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string FLOW_NAME = "Flow Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string CONVERSATION_NAME = "Conversation Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string TYPE = "Type";

        ///<summary>
        /// PARAMETER FORMAT: "Boolean"
        ///</summary>
        public const string HAS_REPORT_TEMPLATE = "Has Report Template";

    }

    public partial class Entities
    {
        public const string CLIENT_FUNCTION_CATEGORY = "Client Function Category";
    }

    public class EntityFieldsForClientFunctionCategory
    {
        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string NAME = "Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string DESCRIPTION = "Description";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string CLIENT_FUNCTION_CATEGORY = "Client Function Category";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string ENTITY = "Entity";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string THEENTITYNAME = "TheEntityName";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string LARGE_ICON_URL = "Large Icon Url";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string SMALL_ICON_URL = "Small Icon Url";

        ///<summary>
        /// PARAMETER FORMAT: "AlphaNumeric"
        ///</summary>
        public const string MAIN_SERVICE_ID = "Main Service ID";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string FBN_IMAGE = "FBN Image";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string FBN_IMAGE_SELECTED = "FBN Image Selected";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string ENTITY_TEMPLATE = "Entity Template";

    }

    public partial class Entities
    {
        public const string ENTITY = "Entity";
    }

    public class EntityFieldsForEntity
    {
        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string NAME = "Name";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string DESCRIPTION = "Description";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string MUST_BE_ACTIVE_IN_PARENT = "Must Be Active In Parent";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string DISPLAY_NAME = "Display Name";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string SERVICE = "Service";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string THEFIELDS = "TheFields";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string MAIN_SERVICE_ID = "Main Service ID";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string IS_GENERAL = "Is General";

    }

    public partial class Entities
    {
        public const string SERVICE = "Service";
    }

    public class EntityFieldsFor
    {
        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string NAME = "Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string DESCRIPTION = "Description";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string SERVICE_CATEGORY = "Service Category";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string MAIN_SERVICE_ID = "Main Service ID";

    }

    public partial class Entities
    {
        public const string SERVICE_CATEGORY = "Service Category";
    }

    public class EntityFieldsForCategory
    {
        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string NAME = "Name";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string CODE = "Code";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string DESCRIPTION = "Description";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string MAIN_SERVICE_ID = "Main Service ID";

    }

    public partial class Entities
    {
        public const string ZONE_MERCHANT_TRANSACTION_FEE = "Zone Merchant Transaction Fee";
    }

    public class EntityFieldsForZoneMerchantTransactionFee
    {
        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string NAME = "Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string FEE_SPLIT_RATIOS = "Fee Split Ratios";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string FEES = "Fees";

    }

    public partial class Entities
    {
        public const string ZONE_BANK = "Zone Bank";
    }

    public class EntityFieldsForZoneBank
    {
        ///<summary>
        /// PARAMETER FORMAT: "Integer"
        ///</summary>
        public const string BANK_CODE = "Bank Code";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string BANK_NAME = "Bank Name";

        ///<summary>
        /// PARAMETER FORMAT: "Boolean"
        ///</summary>
        public const string IS_SETTLEMENT_BANK = "Is Settlement Bank";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string IMAGE_TYPE_FOR_SAVINGS_ACCOUNT = "Image Type for Savings Account";

        ///<summary>
        /// PARAMETER FORMAT: Unknown; maybe an Entity
        ///</summary>
        public const string IMAGE_TYPE_FOR_CURRENT_ACCOUNT = "Image Type for Current Account";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string INSTITUTION_CODE = "Institution Code";

        ///<summary>
        /// PARAMETER FORMAT: "Integer"
        ///</summary>
        public const string CBN_CODE = "CBN Code";

    }

    public partial class Entities
    {
        public const string IMAGE_TYPE = "Image Type";
    }

    public class EntityFieldsForImageType
    {
        ///<summary>
        /// PARAMETER FORMAT: "Integer"
        ///</summary>
        public const string ID = "ID";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string NAME = "Name";

        ///<summary>
        /// PARAMETER FORMAT: "String"
        ///</summary>
        public const string URL = "Url";

    }


    #endregion
}


