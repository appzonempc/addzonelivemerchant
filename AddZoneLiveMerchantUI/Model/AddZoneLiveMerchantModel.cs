﻿using System.Collections.Generic;
using System.Dynamic;
using System.Web;

namespace AddZoneLiveMerchant.Model
{
    /// <summary>
    /// This is just an  Empty PageModel. kds
    /// You will need to populate your page model accordingly
    /// </summary>
    public class AddZoneLiveMerchantModel
    {

        public string MerchantName { get; set; }
        public string MerchantDetails { get; set; }
        public string FriendlyName { get; set; }
        public string WelcomeMessage { get; set; }
        public string UpdateMessage { get; set; }
        public HttpPostedFile Logo { get; set; }
        public bool IsDownloadedAtSetup { get; set; }
        public string BusinessAddress { get; set; }
        public string SupportPhoneNumber { get; set; }
        public string SupportEmailAddress { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmailAddress { get; set; }
        public string ContactLastName { get; set; }
        public string WebsiteUrl { get; set; }
        public string SuccessUrl { get; set; }
        public string FailureUrl { get; set; }
        public string PendingUrl { get; set; }
        public string TerminalList { get; set; }
        public string Fee { get; set; }
        public string SettlementAccountName { get; set; }
        public string SettlementAccountNumber { get; set; }
        public DejaVuObject SettlementAccountBank { get; set; }
        public List<DejaVuObject> Services { get; set; }
        public string ErrorMessage { get; set; }

        public DejaVuObject ToDejaVuObject()
        {
            var objZoneLiveMerchant = new DejaVuObject();

            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.MERCHANT_NAME, MerchantName);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.MERCHANT_DETAILS, MerchantDetails);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.FRIENDLY_NAME, FriendlyName);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.UPDATEMESSAGE, UpdateMessage);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.WELCOMEMESSAGE, WelcomeMessage);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.IS_DOWNLOADED_AT_SETUP, IsDownloadedAtSetup);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.LOGO, Logo);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.BUSINESS_ADDRESS, BusinessAddress);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.SUPPORT_PHONE_NUMBER, SupportPhoneNumber);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.SUPPORT_EMAIL_ADDRESS, SupportEmailAddress);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.CONTACT_EMAIL_ADDRESS, ContactEmailAddress);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.CONTACT_FIRST_NAME, ContactFirstName);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.CONTACT_LAST_NAME, ContactLastName);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.CONTACT_PHONE_NUMBER, ContactPhoneNumber);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.WEBSITE_URL, WebsiteUrl);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.SUCCESS_RETURN_URL, SuccessUrl);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.FAILURE_RETURN_URL, FailureUrl);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.PENDING_RETURN_URL, PendingUrl);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.LIST_OF_TERMINALS, TerminalList);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.FEE, Fee);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.SERVICES, Services);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.SETTLEMENT_ACCOUNT_BANK, SettlementAccountBank);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.SETTLEMENT_ACCOUNT_NAME, SettlementAccountName);
            objZoneLiveMerchant.Set(EntityFieldsForZoneMerchantLive.SETTLEMENT_ACCOUNT_NUMBER, SettlementAccountNumber);

            return objZoneLiveMerchant;
        }
    }
}