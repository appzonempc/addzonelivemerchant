﻿
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DejaVu.ServiceHelper;
using DejaVu.ServiceHelper.Utility;
using DejaVu.ServiceHelper.Logging;
using DejaVu.UIHelper;
using AppZoneUI.Framework;

using AddZoneLiveMerchant.Model;
using DejaVu.TweetConsole;

namespace AddZoneLiveMerchant.View
{
    /// <summary>
    /// Class AddZoneLiveMerchantPage
    /// NB: All the Custom page parameters and the event fields may be assessed here by typing
    /// PageFieldsFor... or EntityFieldsFor... 
    /// </summary>
    public class AddZoneLiveMerchantPage : DejaVuEntityUI<AddZoneLiveMerchantModel>
    {
        private List<DejaVuObject> _assignedTerminals;

        public AddZoneLiveMerchantPage()
        {
            WithTitle("Zone Live Merchant");
            UseFullView();

            AddSection().IsFormGroup().WithTitle("Merchant Information").WithColumns(
                new List<Column>
                {
                    new Column(new List<IField>
                    {
                        Map(x => x.MerchantName).AsSectionField<TextBox>().TextFormatIs(TextFormat.name).Required(),

                        Map(x=> x.Logo).AsSectionField<Upload>().Required().LabelTextIs("Upload Image (*)"),

                         Map(x => x.MerchantDetails).AsSectionField<TextBox>().Required()

                    }),
                    new Column(new List<IField>
                    {
                        Map(x=>x.FriendlyName).AsSectionField<TextBox>().Required(),

                        Map(x=>x.IsDownloadedAtSetup).AsSectionField<CheckBox>(),

                        Map(x=>x.WelcomeMessage).AsSectionField<TextArea>().Required(),

                        Map(x=>x.UpdateMessage).AsSectionField<TextArea>(),
                    })

                });
            AddSection().IsFormGroup().WithTitle("Business Contact Information").WithColumns(
                new List<Column>
            {
                new Column(new List<IField>
                {
                    Map(x => x.BusinessAddress).AsSectionField<TextBox>(),
                }),
                new Column(new List<IField>
                    {
                        Map(x => x.SupportPhoneNumber).AsSectionField<TextBox>().TextFormatIs(TextFormat.naijaGsm).Required(),
                        Map(x => x.SupportEmailAddress).AsSectionField<TextBox>().TextFormatIs(TextFormat.email).Required()
                    })
                });
            AddSection().IsFormGroup().WithTitle("Contact Person Information").WithColumns(new List<Column>
            {
                new Column(new List<IField>
                {
                    Map(x => x.ContactFirstName).AsSectionField<TextBox>().TextFormatIs(TextFormat.name).Required(),
                    Map(x => x.ContactLastName).AsSectionField<TextBox>().TextFormatIs(TextFormat.name).Required()
                }),
                    new Column(new List<IField>
                    {
                        Map(x => x.ContactEmailAddress).AsSectionField<TextBox>().TextFormatIs(TextFormat.email).Required(),
                        Map(x => x.ContactPhoneNumber).AsSectionField<TextBox>().TextFormatIs(TextFormat.naijaGsm).Required()
                    })

    });
            AddSection().IsFormGroup().WithTitle("Web Information").WithColumns(new List<Column>
            {
                new Column(new List<IField>
                {
                    Map(x => x.WebsiteUrl).AsSectionField<TextBox>().TextFormatIs(TextFormat.website),
                    Map(x => x.SuccessUrl).AsSectionField<TextBox>().TextFormatIs(TextFormat.website)
                }),
                new Column(new List<IField>
                {
                    Map(x => x.FailureUrl).AsSectionField<TextBox>().TextFormatIs(TextFormat.website),
                    Map(x => x.PendingUrl).AsSectionField<TextBox>().TextFormatIs(TextFormat.website)
                })
            });
            AddSection().IsFormGroup().WithTitle("Settlement Account Information").WithColumns(new List<Column>
            {
                new Column(new List<IField>
                {
                    Map(x => x.SettlementAccountName).AsSectionField<TextBox>().TextFormatIs(TextFormat.name),
                    Map(x => x.SettlementAccountNumber).AsSectionField<TextBox>().TextFormatIs(TextFormat.numeric),
                }),
                new Column(new List<IField>
                {
                    Map(x => x.SettlementAccountBank).AsSectionField<DropDownList>().Of<DejaVuObject>(()=>GetBy(Entities.ZONE_BANK, new DejaVuObject())).ListOf(x=>x[EntityFieldsForZoneBank.BANK_NAME],x=>x[DejaVuObject.ID]).Required()

                })
            });
            AddSection().IsFormGroup().WithTitle("Terminals").WithColumns(new List<Column>()
            {
                new Column(new List<IField>()
                {
                    Map(x => x.TerminalList).AsSectionField<MultiSelect>().Of(() =>
                    {
                        var terminals = new DejaVuObject();
                        var terminalList = GetBy(Entities.ZONE_TERMINAL, terminals);
                        _assignedTerminals = GetBy(Entities.MERCHANT_TERMINAL, terminals);
                        return terminalList.Where(IsUnAssigned).ToList();
                    })
                        .ListOf(x => Convert.ToString(x.Get(EntityFieldsForZoneTerminal.TERMINAL_NAME)), x => x.Get(EntityFieldsForZoneTerminal.TERMINAL_NAME))
                        .LabelTextIs("Select Terminals:")
                })
            });
            AddSection().IsFormGroup().WithTitle("Services").WithColumns(new List<Column>()
            {
                new Column(new List<IField>()
                {
                    Map(x => x.Services).AsSectionField<MultiSelect>().Of(() =>
                    {
                        var serviceList = GetBy(Entities.ZONE_MERCHANT_SERVICE, new DejaVuObject());
                        return serviceList;
                    })
                        .ListOf(x => Convert.ToString(x.Get(EntityFieldsForZoneMerchantService.NAME)), x => x.Get(EntityFieldsForZoneMerchantService.NAME))
                        .LabelTextIs("Select Merchant Services:")
                })
            });

            AddButton().WithText("Add").SubmitTo(x =>
            {
                var save = false;
                if (string.IsNullOrEmpty(x.MerchantName))
                {
                    x.ErrorMessage = "Merchant Name is Required";
                    return save;
                }
                if (string.IsNullOrEmpty(x.BusinessAddress))
                {
                    x.ErrorMessage = "Business Address is Required";
                    return save;
                }
                if (string.IsNullOrEmpty(x.MerchantDetails))
                {
                    x.ErrorMessage = "Merchant Details is Required";
                    return save;
                }

                if (string.IsNullOrEmpty(x.SupportPhoneNumber))
                {
                    x.ErrorMessage = "Support Phone Number is Required";
                    return save;
                }

                if (string.IsNullOrEmpty(x.SupportEmailAddress))
                {
                    x.ErrorMessage = "Support Email Address is Required";
                    return save;
                }
                if (x.Logo != null && !ValidateFile(x.Logo))
                {
                    x.ErrorMessage = "Logo is invalid";
                    return save;
                }
                var newZoneMerchant = new DejaVuObject()
                {
                    [EventZoneLiveMerchantCreated.ZONE_LIVE_MERCHANT] = x.ToDejaVuObject()
                };

                //eventField[EventZoneLiveMerchantCreated.ZONE_LIVE_MERCHANT] = x.ToDejaVuObject();


                RaiseEvent(CustomEvents.ZONE_LIVE_MERCHANT_CREATED, newZoneMerchant);
                save = true;
                Tweeter.Tweet("AddZoneLiveMerchantPage", EventZoneLiveMerchantCreated.ZONE_LIVE_MERCHANT + " event created raised with object " + GetEntityString(newZoneMerchant));
                return save;
            }).OnFailureDisplay(x => x.ErrorMessage);

            //AddSection().IsFormGroup().WithColumns(new List<Column>
            //{
            //    new Column(new List<IField>
            //    {
            //        Map(x => x.TerminalList).AsSectionField<TextBox>()
            //    }),
            //    new Column(new List<IField>
            //    {
            //        Map(x => x.Fee).AsSectionField<TextBox>().TextFormatIs(TextFormat.money)
            //    })
            //});
            //AddSection().IsFormGroup().WithColumns(new List<Column>
            //{
            //    new Column(new List<IField>
            //    {
            //        Map(x => x.).AsSectionField<TextBox>()
            //    }),
            //    new Column(new List<IField>
            //    {
            //        Map(x => x.PendingUrl).AsSectionField<TextBox>()
            //    })


            //NB: Unlike in Commands, the order in which you implement these Checks and Processings does not have to be serial.
            // In any case, remember to raise the required event(s) at the appropriate place.

            //===============Custom Checks====================

            //===============Processings=====================



            //==============Custom Events====================

            #region Event 1 - Zone Live Merchant created

            //var eventField = new DejaVuObject
            //{
            //    [EventZoneLiveMerchantCreated.ZONE_LIVE_MERCHANT] = x.ToDejaVuObject()
            //};
            //RaiseEvent(CustomEvents.ZONE_LIVE_MERCHANT_CREATED, (DejaVuObject)eventField);
            #endregion

        }

        //==============IV: Custom UI Code====================
        #region Put helper methods here. 
        private bool ValidateFile(HttpPostedFile uploadedLogo)
        {
            if (uploadedLogo == null || uploadedLogo.FileName == string.Empty)
                return false;
            var extension = Path.GetExtension(uploadedLogo.FileName);
            return uploadedLogo.ContentLength <= 1000000 && (extension == ".jpg" || extension == ".png" || extension == ".gif" || extension == ".jpeg");
        }

        private bool IsUnAssigned(DejaVuObject terminal)
        {
            var terminalId = terminal["Terminal Id"] as string;
            foreach (var dejaVuObject in _assignedTerminals)
            {
                string str2 = (dejaVuObject["Terminal"] as DejaVuObject)["Terminal Id"] as string;
                if (terminalId == str2)
                    return dejaVuObject["Merchant"] == null;
            }
            return true;
        }

        public string GetEntityString(DejaVuObject entity)
        {
            StringBuilder result = new StringBuilder();
            foreach (var field in entity)
            {
                string fieldValue = string.Empty; //= Convert.ToString(field.Value);
                if (field.Value is DejaVuObject)
                {
                    result.AppendFormat("{0}:[{1}]\t", field.Key, GetEntityString(field.Value as DejaVuObject));
                }
                else
                {
                    result.AppendFormat("{0}:{1}\t", field.Key, field.Value);
                    Console.WriteLine(string.Format("{0}:{1}\t", field.Key, field.Value));
                }
            }
            return result.ToString();
        }

        #endregion
    }
}
