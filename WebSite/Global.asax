﻿ 
<%@ Application Language="C#" %>
<script runat="server">

    protected void Application_Start(object sender, EventArgs e)
    { 
        DejaVu.WebProductionIntegration.WebBus.Init();
        AppZoneUI.Framework.Extensions.ServiceLocator ssl = new AppZoneUI.Framework.Extensions.ServiceLocator();
        ssl.Register<AppZoneUI.Framework.IRepeatingSection>(() => new AppZoneUI.Framework.RepeatingSection()); 
        Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(() => ssl);
        var map  = new Dictionary<string,Type>();
        map.Add("AddZoneLiveMerchant", typeof(AddZoneLiveMerchant.View.AddZoneLiveMerchantPage));
        AppZoneUI.AjaxRenderers.MapUtility.MapAllRenderers();
        AppZoneUI.Framework.EntityUIRouteHandler.MapRoutes(map);
    }

    protected void Session_Start(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {

    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {

    }

</script> 