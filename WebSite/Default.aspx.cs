﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using AppZoneUI.Framework;
using DejaVu.ServiceHelper;
using DejaVu.WebProductionIntegration;

public partial class _Default : System.Web.UI.Page
{
	BaseCommandProcessor _processor;

	public _Default()
	{
		_processor = new BaseWebCommandProcessor(this);

		bool demoMode = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["DemoMode"]);
		if (demoMode)
		{
			_processor.DejavuApi = new DejaVu.DevelopmentIntegration.DejaVuApi();
			_processor.EntityService = new DejaVu.DevelopmentIntegration.EntityDAO() { CommandProcessor = _processor };
		}
		else
		{
			_processor.DejavuApi = new DejaVu.ProductionIntegration.ProcessorUtils(_processor);
			_processor.EntityService = new DejaVu.ProductionIntegration.DataAccess.EntityDAO() { CommandProcessor = _processor };
		}
	}

	protected override void OnPreInit(EventArgs e)
	{
		base.OnPreInit(e);

		IDictionary<string, object> commandFields = new DejaVuObject();

		form1.Controls.Add(new EntityUIControl()
		{
			ID = "ecCustomUI",
			//Reference the 'AddZoneLiveMerchantUI' project also in this Web project
			TheEntityUI = new AddZoneLiveMerchant.View.AddZoneLiveMerchantPage { TheCommandProcessor = _processor }
		});
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			hfWorkflow.Value = Request.Params["hfWorkflow"];
		}
	}
}
